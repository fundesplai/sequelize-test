const express = require('express');
const router = express.Router();

const modelo = require('../models/index.js');

const multer = require('multer');

// referencia:
//  https://programmingwithmosh.com/javascript/react-file-upload-proper-server-side-nodejs-easy/


//multer es un plugin que facilita la lectura de archivos procedentes de forms
//aquí se inicializa, indicando que la carpeta es 'uploads'
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + '-' + file.originalname)
    }
})

const upload = multer({ storage: storage }).single('file');





//get all
router.get('/', (req, res, next) => {
    modelo.Contacto.findAll()
        .then(lista => res.json({ ok: true, data: lista }))
        .catch(err => res.json({ ok: false, error: err }));
});

// post foto
router.post('/foto', (req, res, next) => {

    upload(req, res, function (err) {
        if (err instanceof multer.MulterError) {
            return res.status(500).json(err)
        } else if (err) {
            return res.status(500).json(err)
        }
        
        console.log(req.file);

        let idcontacto = req.body.idcontacto;
        if (idcontacto){
            modelo.Contacto.findOne({ where: { id: idcontacto } })
                .then(item => {
                    item.urlfoto = req.file.filename;
                    return item.save();
                })
                .then (() => res.status(200).send(req.file));
        } else {
            return res.status(200).send(req.file);
        }

        
    })

});



router.post('/', (req, res, next) => {
    modelo.Contacto.create(req.body)
        .then(item => res.json({ ok: true, data: item }))
        .catch(err => res.json({ ok: false, error: err }));
});



router.get('/:id', (req, res, next) => {
    let idcontacto = req.params.id;
    // modelo.Contacto.findById(idcontacto)
    modelo.Contacto.findOne({ where: { id: idcontacto } })
        .then(item => res.json({ ok: true, data: item }))
        .catch(err => res.json({ ok: false, error: err }));
});


router.put('/:id', (req, res, next) => {
    let idcontacto = req.params.id;
    // modelo.Contacto.findById(idcontacto)
    modelo.Contacto.findOne({ where: { id: idcontacto } })
        .then(item => item.update(req.body))
        .then(item => res.json({ ok: true, data: item }))
        .catch(err => res.json({ ok: false, error: err }));
});



router.delete('/:id', (req, res, next) => {
    let idcontacto = req.params.id;
    modelo.Contacto.destroy({ where: { id: idcontacto } })
        .then(item => res.json({ ok: true, data: item }))
        .catch(err => res.json({ ok: false, error: err }));
});


module.exports = router;