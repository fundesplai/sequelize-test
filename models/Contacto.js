'use strict';

module.exports = (sequelize, DataTypes) => {
  const Contacto = sequelize.define('Contacto', {
   
    nombre: DataTypes.STRING,
    email: DataTypes.STRING,
    urlfoto: DataTypes.STRING,
    piefoto: DataTypes.STRING,
    ciudad: DataTypes.STRING,
  
    
  }, { tableName: 'contactos', timestamps: true});
  
  return Contacto;
};


